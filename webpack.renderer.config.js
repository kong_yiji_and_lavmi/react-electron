const rules = require("./webpack.rules");
const path = require("path");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const WebpackRouterGenerator = require("webpack-router-generator");
process.env.NODE_ENV = "development";
module.exports = {
  module: {
    rules,
  },
  target: "electron-renderer",
  resolve: {
    alias: {
      "@": path.join(__dirname, "./src"),
      assets: path.join(__dirname, "./src/assets"),
      "~": path.join(__dirname),
    },
    preferRelative: true,
    extensions: [".ts", ".tsx", "..."],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new WebpackRouterGenerator({
      KeyWord: "route",
      fileDir: path.join(__dirname, "./src/pages"),
      comKey: "component",
      outputFile: path.join(__dirname, "./src/router/list.js"),
      exts: [".js", ".jsx", ".tsx"],
    }),
  ],
};

import * as ReactDOM from "react-dom";
import React from "react";
import { HashRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import "antd/dist/antd.css";
import "assets/css/global.less";
import Layout from "./layout";
import { message } from "antd";
message.config({
  maxCount: 3,
  duration: 2,
  top: 50
});
ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <Layout />
    </HashRouter>
  </Provider>,
  document.getElementById("app")
);

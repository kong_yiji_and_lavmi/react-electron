import React from "react";


export interface IconProps extends React.HTMLAttributes<HTMLDivElement> {
  type: string | null
}

export function Icon({ type, ...itemProps }: IconProps) {
  if (!type) return null;
  return <i className={"iconfont icon-" + type} {...itemProps} />;
}

import React from "react"
import { Spin, Progress, Typography, Button } from "antd"
import { connect } from "react-redux";
import { Deal, StoreState, Dispatch, } from "@/types";
import "./index.less"
import { SETDEALSHOW } from "@/store/types";
export interface DealLoaddingProps extends Deal {
  children?: JSX.Element
  setDealShow: (isShow: boolean) => void
}

type TipInfoProps = Omit<DealLoaddingProps, "children">
const { Title, Text } = Typography

function getPercent(num1: number, num2: number) {
  return Number(((num1 / num2) * 100).toFixed(2))
}

function TipInfo(props: TipInfoProps) {
  return <div className="tip-info">
    <Title level={3}>当前进度：</Title>
    <Progress type="circle" percent={getPercent(props.fail + props.success, props.total)} />
    <p>
      <Text>总量：<span className="num fail-text">{props.total}</span></Text>
    </p>
    <p>
      <Text>成功：<span className="num success-text">{props.success}</span></Text>
    </p>
    <p>
      <Text>失败：<span className="num fail-text">{props.fail}</span></Text>
    </p>
    <p>
      {!props.isDeal && <Button type="primary" onClick={() => props.setDealShow(false)} >关闭</Button>}
    </p>
  </div>
}
function DealLoaddingCom({ children, ...anyProps }: DealLoaddingProps) {
  return <Spin className="deal-loadding" indicator={null} spinning={anyProps.isShow} tip={<TipInfo {...anyProps} />}>
    {children}
  </Spin>
}
const mapStateToProps = (state: StoreState) => ({
  ...state.deal
})
const mapDispatchToProps = (dispatch: Dispatch) => ({
  setDealShow: (isShow: boolean) => dispatch({ type: SETDEALSHOW, isShow })
})
export const DealLoadding: React.FC<any> = connect(mapStateToProps, mapDispatchToProps)(DealLoaddingCom)
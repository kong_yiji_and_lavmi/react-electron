
import React from "react";
import { CardProps, Card } from "antd"
import { CompressFile, CurrentFile } from "types/index"
import "./index.less"


interface FileInfo extends
  Omit<CurrentFile, "checked" | "oldSizeStr" | "oldSize" | "relativePath" | "birthtime">,
  Omit<CompressFile, 'oldSize' | "oldSizeStr"> {
  checked?: boolean
  oldSizeStr?: string
  oldSize?: number
  relativePath?: string
  birthtime?: string
}

export interface ImgCardProps extends Omit<CardProps, "onClick"> {
  fileInfo: FileInfo,
  onClick?: (path: string, checked: boolean) => void
}


export interface CompressImgCardProps extends CardProps {
  fileInfo: CompressFile
}
export function CompressImgCard({ fileInfo }: CompressImgCardProps) {
  return <Card
    hoverable
    className="img-card"
    title={<p className=" text-over" title={fileInfo.name}>
      {fileInfo.name}
    </p>}
    cover={<img draggable={false} src={fileInfo.path} alt={fileInfo.name} />}
  >
    <p>
      <span className="label">原文件大小：</span>
      <span className="old">{fileInfo.oldSizeStr}</span>
    </p>
    <p>
      <span className="label">现文件大小：</span>
      <span className="new">{fileInfo.sizeStr}</span>
    </p>
  </Card>
}

export function ImgCard({ fileInfo, ...props }: ImgCardProps) {
  return <Card
    {...props}
    hoverable
    className={"img-card" + (fileInfo.checked ? ' checked' : '')}
    title={<p className=" text-over" title={fileInfo.name}>
      {fileInfo.name}
    </p>}
    cover={<img draggable={false} src={fileInfo.path} alt={fileInfo.name} />}
    onClick={() => props.onClick && props.onClick(fileInfo.path, fileInfo.checked)}
  >
    <p>
      <span className="label">文件大小：</span>
      {fileInfo.sizeStr}
    </p>
  </Card>
}

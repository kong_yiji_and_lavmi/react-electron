import { SETFILELIST, SETFILELOADDING, SETFILEINITSTATE } from "../types";
import { FileState, FileStateAction } from "@/types/store"
const initState: FileState = {
  loadding: false,
  list: [],
};
export default function reducer(state = initState, action: FileStateAction) {
  const { type, list, loadding } = action;
  switch (type) {
    case SETFILELIST:
      return { ...state, list };
    case SETFILELOADDING:
      return { ...state, loadding };
    case SETFILEINITSTATE:
      return initState
    default:
      return state;
  }
}

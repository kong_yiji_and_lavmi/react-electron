import { SETCHOOSEDIR, SETOPEN, SETCHOOSEDIRINITSTATE, SETOUTPUTDIR } from "../types";
import { DirState, DirStateAction } from "@/types/store"
const initState: DirState = {
  isopen: false,
  chooseDir: null,
  outputDir: null
};
export default function reducer(state = initState, action: DirStateAction) {
  const { chooseDir, type, isopen, outputDir } = action;
  switch (type) {
    case SETCHOOSEDIR:
      return { ...state, chooseDir };
    case SETOPEN:
      return { ...state, isopen };
    case SETOUTPUTDIR:
      return { ...state, outputDir }
    case SETCHOOSEDIRINITSTATE:
      return initState
    default:
      return state;
  }
}

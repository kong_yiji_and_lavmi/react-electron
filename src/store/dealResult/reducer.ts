import { Deal, DealActionType } from "@/types"
import { SETDEALRESULT, SETDEALSHOW, SETINNTDEALRESULT } from "../types"
const initState: Deal = {
  success: 0,
  fail: 0,
  total: 0,
  isShow: false,
  isDeal: false
}
export default function reducer(state = initState, action: DealActionType) {
  const { type, result, isShow } = action
  switch (type) {
    case SETDEALRESULT:
      return result
    case SETDEALSHOW:
      return { ...state, isShow }
    case SETINNTDEALRESULT:
      return initState
    default:
      return state
  }
}
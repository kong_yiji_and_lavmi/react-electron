import { SIZETYPE } from "../types";
import { WindowSizeAction, WindowSizeState } from "@/types/store"
export default function reducer(state: WindowSizeState = "normal", action: WindowSizeAction) {
  const { size, type } = action;
  switch (type) {
    case SIZETYPE:
      return size;
    default:
      return state;
  }
}

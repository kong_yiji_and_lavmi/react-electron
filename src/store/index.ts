import { createStore, combineReducers } from "redux";
import windowSize from "./windowSize/reducer";
import dir from "./dir/reducer";
import file from "./file/reducer";
import deal from "./dealResult/reducer"
const reducer = combineReducers({
  windowSize,
  dir,
  file,
  deal
});

const store = createStore(reducer);

export default store;

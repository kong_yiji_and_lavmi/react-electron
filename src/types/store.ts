import { FileInfo, DealResult } from "./electron"
export { FileInfo, DealResult } from "./electron"
export type { Dispatch } from "redux"
export type WindowSizeActionType = "SIZETYPE" | string
export type WindowSizeState = "normal" | 'big' | 'hide' | 'auto' | 'close'
// 窗口
export interface WindowSizeAction {
  type: WindowSizeActionType
  size: WindowSizeState
}
// 文件列表
export interface FileState {
  loadding: boolean
  list: FileInfo[]
}
export type FileActionType = "SETFILELOADDING" | "SETFILELIST" | string
export interface FileStateAction extends FileState {
  type: FileActionType
}
// 文件夹
export interface DirState {
  isopen: boolean
  chooseDir: string | null
  outputDir: string | null
}
export type DirActionType = "SETCHOOSEDIR" | 'SETOPEN' | string

export interface DirStateAction extends DirState {
  type: DirActionType
}

// 处理结果
export interface Deal extends Omit<DealResult, "successList" | "failList"> {
  isShow: boolean
}
export interface DealActionType {
  type: string
  result: Deal
  isShow: boolean
}
export default interface StoreState {
  dir: DirState
  file: FileState
  windowSize: WindowSizeState
  deal: Deal
}
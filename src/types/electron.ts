import { CurrentFile } from "./index"
export interface FileInfo {
  name: string
  relativePath: string
  path: string
  size: number
  birthtime: string
  sizeStr: string
  extname?: string
}

export interface DealRequire {
  position: string,
  quality: number,
  isRelative: boolean
}

export interface CompressFile {
  path: string,
  size: number,
  name: string,
  sizeStr: string
  oldSizeStr: string
  oldSize: number
}
export interface DealResult {
  success: number
  fail: number
  successList: Array<CompressFile>
  failList: Array<CompressFile>
  total: number,
  isDeal: boolean
}

export interface CompressResult {
  isSuccess: boolean
  output: string
}
export type CompressFunction = (file: FileInfo | CurrentFile, positionPath: string, quality: number, isRelative: boolean) => Promise<CompressResult>

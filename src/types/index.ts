import { FileInfo } from "./electron"


export interface CurrentFile extends FileInfo {
  checked: boolean
}
export type CurrentFileList = CurrentFile[]

export * from "./electron"
export * from "./store"

export { default as StoreState } from "./store"
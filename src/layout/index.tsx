import React from "react";
import { Layout } from "antd";
import MyHeader from "./header";
import SilderBar from "./silderBar";
import MyConetnt from "./conent";
import listen from "@/listen";
import { DealLoadding } from "@/components";
import "./index.less";
function MyLayout() {
  return (
    <Layout className="app-wrapper">
      <MyHeader />
      <DealLoadding>
        <Layout>
          <SilderBar />
          <MyConetnt />
        </Layout>
      </DealLoadding>
    </Layout>
  );
}

export default listen(MyLayout);

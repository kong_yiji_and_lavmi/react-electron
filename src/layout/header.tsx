import React from "react";
import { Icon } from "@/components";
import { connect } from "react-redux";
import { ipcRenderer } from "electron";
import { Affix } from "antd"
import StoreState, { WindowSizeState } from "@/types/store"
interface Props {
  windowSize: WindowSizeState
}
function MyHeader({ windowSize }: Props) {
  const zoom = (size: WindowSizeState) => {
    ipcRenderer.send("setWindow", size);
  };
  return (
    <Affix offsetTop={0}>
      <div className="header">
        <div className="title">
          <span>本地图片压缩工具</span>
        </div>
        <div className="right">
          <Icon
            type="zuixiaohua success"
            onClick={() => zoom("hide")}
            title="最小化"
          />
          {windowSize === "normal" && (
            <Icon
              type="zuidahua1 warn"
              title="最大化"
              onClick={() => zoom("big")}
            />
          )}
          {windowSize === "big" && (
            <Icon
              type="normal warn"
              title="还原"
              onClick={() => zoom("normal")}
            />
          )}
          <Icon type="guanbi error" title="关闭" onClick={() => zoom("close")} />
        </div>
      </div>
    </Affix>

  );
}
const mapStateToProps = ({ windowSize }: StoreState) => ({
  windowSize,
});

export default connect(mapStateToProps, null)(MyHeader);

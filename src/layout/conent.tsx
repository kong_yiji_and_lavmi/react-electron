import { Layout } from "antd";
import React from "react";
const { Content } = Layout;
import AppRouter from "@/router";

export default function MyConetnt() {
  return (
    <Content className="my-content">
      <AppRouter />
    </Content>
  );
}

import React from "react";
import { Layout } from "antd";
import { NavLink } from "react-router-dom";
const { Sider } = Layout;
export default function SilderBar() {
  return (
    <Sider className="my-slider" width={100}>
      <ul>
        <li>
          <NavLink to="/">批量处理</NavLink>
        </li>
        <li>
          <NavLink to="/single"> 单一处理</NavLink>
        </li>
      </ul>
    </Sider>
  );
}

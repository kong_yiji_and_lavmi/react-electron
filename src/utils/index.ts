import { relative } from "path"
export function isChildPath(childPath: string, parentPath: string): boolean {
  return !/^\.+/.test(relative(childPath, parentPath))
}
import React, { useEffect } from "react";
import store from "@/store";
import * as TYPE from "@/store/types";
import { ipcRenderer, OpenDialogReturnValue } from "electron";
import { message } from "antd";
type NoticeType = 'info' | 'success' | 'error' | 'warning' | 'loading';
export default function listen(Com: React.FC<any>) {
  function WarpperComponents() {
    useEffect(() => {
      // 设置窗口 大小
      ipcRenderer.on("windowSize", (e, size) => {
        store.dispatch({ size: size, type: TYPE.SIZETYPE });
      });
      // 监听 主进程消息
      ipcRenderer.on("message", (e, type: NoticeType, text: string) => {
        message[type](text);
      });
      // 文件夹打开状态
      ipcRenderer.on("openStatus", (e, isopen) => {
        store.dispatch({ type: TYPE.SETOPEN, isopen });
      });
      // 设置选中文件夹
      ipcRenderer.on("setDirState", (e, { filePaths }: OpenDialogReturnValue, setStateType, setStateKey) => {
        if (setStateKey === "chooseDir") {
          store.dispatch({ type: TYPE.SETFILELIST, list: [] })
        }
        store.dispatch({ type: setStateType, [setStateKey]: filePaths[0] || null });
      });
      // 设置 获取的图片列表
      ipcRenderer.on("setFileList", (e, list) => {
        store.dispatch({ type: TYPE.SETFILELIST, list });
      });
      // 设置获取图片列表 加载状态
      ipcRenderer.on("setFileLoad", (e, loadding) => {
        store.dispatch({ type: TYPE.SETFILELOADDING, loadding });
      });
      ipcRenderer.on("compressResult", (e, { successList, failList, ...anyres }) => {
        store.dispatch({ type: TYPE.SETDEALRESULT, result: anyres })
      })
    }, []);
    return <Com />;
  }
  return WarpperComponents;
}
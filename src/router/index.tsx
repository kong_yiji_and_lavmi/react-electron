import * as React from "react";
import { Routes, Route } from "react-router-dom";
import loadable from "@loadable/component";
import list from "./list";
// 页面路由
const Routers = list.map((item) => {
  const Com = loadable(item.component) as React.FC<any>;
  return <Route path={item.path} key={item.path} element={<Com />} />;
});
// 项目路由
export default function AppRouter() {
  return <Routes>{Routers}</Routes>;
}

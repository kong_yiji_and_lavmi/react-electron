const relocateLoader = require('@vercel/webpack-asset-relocator-loader');
module.exports = {
  /**
   * This is the main entry point for your application, it's the first file
   * that runs in the main process.
   */
  entry: "./main.ts",
  // Put your normal webpack config below here
  target: "electron-main",

  module: {
    rules: require("./webpack.rules"),
  },
  resolve: {
    extensions: [".ts", "..."],
  },
  plugins: [
    {
      apply(compiler) {
        compiler.hooks.compilation.tap(
          'webpack-asset-relocator-loader',
          (compilation) => {
            relocateLoader.initAssetCache(compilation, '');
          },
        );
      },
    },
  ]
};

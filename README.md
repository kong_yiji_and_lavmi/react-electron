# react-electron（名字没想好）

本桌面软件是用来批量压缩`jpg,png`内存大小。仅供学习参考

## 技术支持

- [electron](https://www.electronjs.org/zh/docs/latest/tutorial/quick-start) 桌面应用开发框架
- [electron-forge](https://www.electronforge.io/) 桌面应用手脚架
- [webpack5](https://www.webpackjs.com/) 打包构建工具
- [React](https://react.docschina.org/) 前端框架
- [AntdUI](https://ant-design.gitee.io/index-cn) 前端 UI 框架
- [TypeScript](https://www.tslang.cn/) JavaScript 超集
- [ESLint](http://eslint.cn/) 可组装的 JavaScript 和 JSX 检查工具
- [nodejs](http://nodejs.cn/) 是一个基于 Chrome V8 引擎的 JavaScript 运行时。

## 使用

1. 下载当前项目

```bash
D:>git clone https://gitee.com/kong_yiji_and_lavmi/react-electron
```

2. 安装项目所需依赖

```bash
D:\react-electron> npm i
# or
D:\react-electron> cnpm i
# or
D:\react-electron> yarn install
```

3. 启动

```bash
D:\react-electron> npm run start
```

> 等待打包完成会弹出桌面应用窗口就可以正常使用了。若出现未知问题还需仔细交流。

## 打包

1. 使用`npm run package` 直接打包软件,生成可执行 `exe` 文件

```bash
D:\react-electron> npm run package
```

2. 使用`npm run make`或 `npm run publish` 直接打包软件,生成可执行 `exe` 文件 和`可执行安装包文件exe`

```bash
D:\react-electron> npm run make
# or
D:\react-electron> npm run publish
```

## 提示

若安装依赖出现 `pngquant-bin` 抛出错误 请直接忽略。

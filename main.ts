import { app, BrowserWindow } from 'electron';
import listener from "./electron/listener"
declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
if (require("electron-squirrel-startup")) {
  app.quit();
}

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      webSecurity: false,
    },
    frame: false,
    titleBarStyle: "customButtonsOnHover",
    show: false,
  });
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
  process.env.NODE_ENV === "development" && mainWindow.webContents.openDevTools();
  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
  });

  // 开启子进程通信监听
  listener(mainWindow);
};

// 主进程启动 开启一个弹窗
app.on("ready", createWindow);

// 所有窗口关闭 结束主进程
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // 主进程启动 如果没有打开窗口 就打开一个窗口
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
